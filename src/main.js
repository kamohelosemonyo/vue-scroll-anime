const animate = 'anime'
const animateActive = 'anime-active'

function scrollSpy (callback) {
  window.addEventListener('scroll', function () {
    callback()
  })
}

function animateElements (element, modifiers) {
  if (isElementInView(element)) {
    element.classList.add(animateActive)
  } else if (modifiers.repeat) {
    element.classList.remove(animateActive)
  }
}

function isElementInView (element) {
  const rect = element.getBoundingClientRect()
  return (
    rect.top >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)
  )
}

const directive = {
  mounted (el, binding) {
    el.classList.add(animate)
    el.classList.add(binding.value)
    const modifiers = binding.modifiers

    scrollSpy(function () {
      animateElements(el, modifiers)
    })
  }
}

exports.VueScrollAnime = {
  install: (app, options) => {
    app.directive('scroll-anime', directive)
  }
}
